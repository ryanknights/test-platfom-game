//
//  GameScene.swift
//  Test Platform
//
//  Created by Ryan Knights on 18/11/2016.
//  Copyright © 2016 Ryan Knights. All rights reserved.
//

import SpriteKit
import GameplayKit

let BorderCategory    : UInt32 = 0x1 << 0
let GroundCategory    : UInt32 = 0x1 << 1
let PlayerCategory    : UInt32 = 0x1 << 2
let SceneEdgeCategory : UInt32 = 0x1 << 3
let PitCategory       : UInt32 = 0x1 << 4
let PitEntryCategory  : UInt32 = 0x1 << 5
let EnemyCategory     : UInt32 = 0x1 << 6

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var jumpBtn: SKSpriteNode! = nil
    var leftBtn: SKSpriteNode! = nil
    var rightBtn: SKSpriteNode! = nil
    var livesLabel: SKLabelNode! = nil
    
    var player: SKSpriteNode! = nil
    var playerMovement: SKAction! = nil
    var playerStartPosition: CGPoint! = nil
    var playerLives = 3
    var playerIsDying = false
    
    var skyBg: SKSpriteNode! = nil
    var skyBgNext: SKSpriteNode! = nil
    var mountainsBg: SKSpriteNode! = nil
    var mountainsBgNext: SKSpriteNode! = nil
    var groundBg: SKSpriteNode! = nil
    
    var pit: SKSpriteNode! = nil
    var pitEntry: SKSpriteNode! = nil
    
    var bgContainer: SKNode! = nil

    var shouldMoveLeft: Bool = false
    var shouldMoveRight: Bool = false
    
    var gameCamera: SKCameraNode = SKCameraNode()
    var cameraMoveXStart: CGFloat = 0
    var cameraMoveXEnd: CGFloat = 0
    
    var lastUpdate: TimeInterval = 0
    var deltaTime: CGFloat = 0.01666
    var lastPlayerPosition: CGPoint! = nil

    var canJump: Bool = false
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = CGVector(dx: 0.0, dy: -9.0)
        addGround()
        addWalls()
        addPlayer()
        addControls()
        addLivesLabel()
        addPits()
        addParallaxBackground()
        addEnemy()
        
        gameCamera = childNode(withName: "//gameCamera") as! SKCameraNode
        setCameraRestraints()

        view.showsFPS = true
        view.showsNodeCount = true
        view.showsPhysics = true
    }
    
    func setCameraRestraints() {
        guard let camera = camera else { return }
        
        // Constrain the camera to stay a constant distance of 0 points from the player node.
        let playerLocationConstraint = SKConstraint.distance(SKRange(constantValue: 0.0), to: player)
        
        /*
         Also constrain the camera to avoid it moving to the very edges of the scene.
         First, work out the scaled size of the scene. Its scaled height will always be
         the original height of the scene, but its scaled width will vary based on
         the window's current aspect ratio.
         */
        let scaledSize = CGSize(width: size.width * camera.xScale, height: size.height * camera.yScale)
        
        /*
         Find the root "board" node in the scene (the container node for
         the level's background tiles).
         */
        let boardNode = childNode(withName: "//world")! as SKNode
        
        /*
         Calculate the accumulated frame of this node.
         The accumulated frame of a node is the outer bounds of all of the node's
         child nodes, i.e. the total size of the entire contents of the node.
         This gives us the bounding rectangle for the level's environment.
         */
        let boardContentRect = boardNode.calculateAccumulatedFrame()
        
        /*
         Work out how far within this rectangle to constrain the camera.
         We want to stop the camera when we get within 100pts of the edge of the screen,
         unless the level is so small that this inset would be outside of the level.
         */
        let xInset = min((scaledSize.width / 2), boardContentRect.width / 2)
        let yInset = min((scaledSize.height / 2), boardContentRect.height / 2)
        
        // Use these insets to create a smaller inset rectangle within which the camera must stay.
        let insetContentRect = boardContentRect.insetBy(dx: xInset, dy: yInset)
        
        // Define an `SKRange` for each of the x and y axes to stay within the inset rectangle.
        let xRange = SKRange(lowerLimit: insetContentRect.minX, upperLimit: insetContentRect.maxX)
        let yRange = SKRange(lowerLimit: 0, upperLimit: 0)
        cameraMoveXStart = xRange.lowerLimit
        cameraMoveXEnd = xRange.upperLimit
        
        // Constrain the camera within the inset rectangle.
        let levelEdgeConstraint = SKConstraint.positionX(xRange, y: yRange)
        levelEdgeConstraint.referenceNode = boardNode
        
        /*
         Add both constraints to the camera. The scene edge constraint is added
         second, so that it takes precedence over following the `PlayerBot`.
         The result is that the camera will follow the player, unless this would mean
         moving too close to the edge of the level.
         */
        camera.constraints = [playerLocationConstraint, levelEdgeConstraint]
    }
    
    func addParallaxBackground () {
        bgContainer = childNode(withName: "//bgContainer")
        skyBg = childNode(withName: "//skyBg") as! SKSpriteNode
        mountainsBg = childNode(withName: "//mountainsBg") as! SKSpriteNode
        //groundBg = childNode(withName: "//groundBg") as! SKSpriteNode
        
        skyBgNext = skyBg.copy() as! SKSpriteNode
        skyBgNext.position = CGPoint(x: (skyBg.position.x + skyBg.size.width) , y: skyBg.position.y)
        bgContainer.addChild(skyBgNext)
        
        mountainsBgNext = mountainsBg.copy() as! SKSpriteNode
        mountainsBgNext.position = CGPoint(x: (mountainsBg.position.x + mountainsBg.size.width), y: mountainsBg.position.y)
        bgContainer.addChild(mountainsBgNext)
    }
    
    func addEnemy() {
        
        enumerateChildNodes(withName: "//enemy", using: {
            enemy, stop in
            
            let enemyNode = enemy as! SKSpriteNode
            
            enemyNode.physicsBody = SKPhysicsBody(rectangleOf: enemyNode.size)
            enemyNode.physicsBody?.isDynamic = true
            enemyNode.physicsBody?.allowsRotation = false
            enemyNode.physicsBody?.categoryBitMask = EnemyCategory
            enemyNode.physicsBody?.contactTestBitMask = GroundCategory | SceneEdgeCategory | PitCategory | PlayerCategory | PitEntryCategory
            enemyNode.physicsBody?.collisionBitMask = GroundCategory | SceneEdgeCategory | PlayerCategory
            
            let enemyStartPosition = enemyNode.position
            let moveAction = SKAction.repeatForever(SKAction.sequence([
                SKAction.moveTo(x: enemyStartPosition.x - 500, duration: 5),
                SKAction.moveTo(x: enemyStartPosition.x, duration: 5)
            ]))
            
            enemyNode.run(moveAction)
        })
    }
    
    func addPlayer() {
        player = childNode(withName: "//player") as! SKSpriteNode
        player.physicsBody = SKPhysicsBody(rectangleOf: player.size)
        
        player.physicsBody?.restitution = 0
        player.physicsBody?.friction = 1
        player.physicsBody?.allowsRotation = false
        player.physicsBody?.mass = 1
        
        player.physicsBody?.categoryBitMask = PlayerCategory
        player.physicsBody?.contactTestBitMask = GroundCategory | SceneEdgeCategory | PitCategory | PitEntryCategory | EnemyCategory
        player.physicsBody?.collisionBitMask = GroundCategory | SceneEdgeCategory | EnemyCategory
        
        var textures:[SKTexture] = []
        for i in 1...8 {
            let imageFileName = String(format: "hopper-%d.png", i)
            textures.append(SKTexture(imageNamed: imageFileName))
        }
        
        playerMovement = SKAction.repeatForever(SKAction.sequence([
            SKAction.animate(with: textures, timePerFrame: 0.025),
            SKAction.animate(with: textures.reversed(), timePerFrame: 0.025)
        ]))
        
        lastPlayerPosition = player.position
        playerStartPosition = player.position
    }
    
    func addWalls() {
        let leftWall = childNode(withName: "//leftBorder") as! SKSpriteNode
        leftWall.physicsBody?.categoryBitMask = SceneEdgeCategory
        leftWall.physicsBody?.contactTestBitMask = PlayerCategory | EnemyCategory
        
        let rightWall = childNode(withName: "//rightBorder") as! SKSpriteNode
        rightWall.physicsBody?.categoryBitMask = SceneEdgeCategory
        rightWall.physicsBody?.contactTestBitMask = PlayerCategory | EnemyCategory
    }
    
    func addGround() {
        let ground: SKSpriteNode = childNode(withName: "//ground") as! SKSpriteNode
        
        ground.physicsBody = SKPhysicsBody(texture: ground.texture!, size: ground.size)
        ground.physicsBody?.isDynamic = false
        ground.physicsBody?.restitution = 0
        ground.physicsBody?.friction = 1

        ground.physicsBody?.categoryBitMask = GroundCategory
        ground.physicsBody?.collisionBitMask = PlayerCategory | EnemyCategory
    }
    
    func addPits () {
        
        enumerateChildNodes(withName: "//pit", using: {
            pit, stop in
            
            let pitNode = pit as! SKSpriteNode
            pitNode.physicsBody = SKPhysicsBody(rectangleOf: pitNode.size)
            pitNode.physicsBody?.isDynamic = false
            pitNode.physicsBody?.categoryBitMask = PitCategory
            pitNode.physicsBody?.collisionBitMask = PlayerCategory | EnemyCategory
            pitNode.physicsBody?.contactTestBitMask = PlayerCategory | EnemyCategory
            
            let pitEntry = pitNode.childNode(withName: "pitEntry") as! SKSpriteNode
            
            pitEntry.physicsBody = SKPhysicsBody(rectangleOf: pitEntry.size)
            pitEntry.physicsBody?.isDynamic = false
            pitEntry.physicsBody?.categoryBitMask = PitEntryCategory
            pitEntry.physicsBody?.collisionBitMask = 0
            pitEntry.physicsBody?.contactTestBitMask = PlayerCategory | EnemyCategory
            
        })
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if firstBody.categoryBitMask == GroundCategory && secondBody.categoryBitMask == PlayerCategory {
            print("Did begin GroundCategory & Player")
            canJump = true
            if (shouldMoveLeft || shouldMoveRight) {
                player.run(playerMovement, withKey: "playerRunning")
            }
        }
        
        if firstBody.categoryBitMask == PlayerCategory && secondBody.categoryBitMask == SceneEdgeCategory {
            print("Did begin SceneEdge & Player")
        }
        
        if firstBody.categoryBitMask == PlayerCategory && secondBody.categoryBitMask == PitCategory {
            print("Did begin Pit & Player")
        }
        
        if firstBody.categoryBitMask == PlayerCategory && secondBody.categoryBitMask == PitEntryCategory {
            print("Did begin PitEntry & Player")
            player.physicsBody?.collisionBitMask = 0
            shouldMoveLeft = false
            shouldMoveRight = false
            
            let delay = SKAction.wait(forDuration: 2.0)
            
            player.run(delay) {
                self.playerReset()
            }
        }
        
        if firstBody.categoryBitMask == PitEntryCategory && secondBody.categoryBitMask == EnemyCategory {
            print("Did begin PitEntry & Enemy")
            let enemy = secondBody.node as! SKSpriteNode
            enemy.physicsBody?.collisionBitMask = 0
            enemy.removeFromParent()
        }
        
        if firstBody.categoryBitMask == PlayerCategory && secondBody.categoryBitMask == EnemyCategory {
            if (!playerIsDying) {
                let enemy = secondBody.node as! SKSpriteNode
                let playerBottom = player.position.y - (player.size.height/2)
                let enemyTop = enemy.position.y + (enemy.size.height/2)
                
                if (playerBottom > enemyTop) {
                    player.physicsBody?.velocity.dy = 0
                    player.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 500))
                    enemy.removeFromParent()
                } else {
                    print("Take a hit")
                    shouldMoveRight = false
                    shouldMoveLeft = false
                    playerLives = playerLives - 1
                    livesLabel.text = "Lives \(playerLives)"
                    player.physicsBody?.applyImpulse(CGVector(dx: -300, dy: 550))
                    
                    if (playerLives <= 0) {
                        
                        playerIsDying = true
                        player.physicsBody?.collisionBitMask = 0
                        player.physicsBody?.contactTestBitMask = 0
                        
                        let delay = SKAction.wait(forDuration: 2.0)
                        
                        player.run(delay) {
                            self.playerIsDying = false
                            self.playerReset()
                        }
                    }
                }
            }
        }
    }
    
    func didEnd(_ contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if firstBody.categoryBitMask == GroundCategory && secondBody.categoryBitMask == PlayerCategory {
            print("Did end GroundCategory & Player")
            canJump = false
        }
        
        if firstBody.categoryBitMask == PlayerCategory && secondBody.categoryBitMask == PitCategory {
            print("Did begin Pit & Player")
            player.physicsBody?.allowsRotation = false
        }
    }
    
    func addControls() {
        jumpBtn = childNode(withName: "//jumpBtn") as! SKSpriteNode
        leftBtn = childNode(withName: "//moveLeftBtn") as! SKSpriteNode
        rightBtn = childNode(withName: "//moveRightBtn") as! SKSpriteNode
        
        let buttonYPosition = positionFromBottom(CGPoint(x: 0, y: 45))
        
        jumpBtn.position.y = buttonYPosition.y
        leftBtn.position.y = buttonYPosition.y
        rightBtn.position.y = buttonYPosition.y
    }
    
    func addLivesLabel () {
        livesLabel = childNode(withName: "//livesLabel") as! SKLabelNode
        let livesPosition = positionFromBottom(CGPoint(x: 0, y: 45))
        livesLabel.position.y = livesPosition.y
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.location(in: gameCamera)
            
            if jumpBtn.contains(location) {
                playerJump()
            }
            
            if leftBtn.contains(location) {
                player.xScale = -1.927
                player.run(playerMovement, withKey: "playerRunning")
                shouldMoveLeft = true
            }
            
            if rightBtn.contains(location) {
                player.xScale = 1.927
                player.run(playerMovement, withKey: "playerRunning")
                shouldMoveRight = true
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.location(in: gameCamera)
            
            if !leftBtn.contains(location) && shouldMoveLeft {
                shouldMoveLeft = false
                player.physicsBody?.velocity.dx = 0
                player.texture = SKTexture(imageNamed: "hopper-1.png")
                player.removeAction(forKey: "playerRunning")
            }
            
            if !rightBtn.contains(location) && shouldMoveRight {
                shouldMoveRight = false
                player.physicsBody?.velocity.dx = 0
                player.texture = SKTexture(imageNamed: "hopper-1.png")
                player.removeAction(forKey: "playerRunning")
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.location(in: gameCamera)
            
            if leftBtn.contains(location) {
                shouldMoveLeft = false
                player.physicsBody?.velocity.dx = 0
                player.texture = SKTexture(imageNamed: "hopper-1.png")
                player.removeAction(forKey: "playerRunning")
            }
            
            if rightBtn.contains(location) {
                shouldMoveRight = false
                player.physicsBody?.velocity.dx = 0
                player.texture = SKTexture(imageNamed: "hopper-1.png")
                player.removeAction(forKey: "playerRunning")
            }
        }
    }
    
    func playerJump() {
        guard canJump == true else {
            return
        }
        
        player.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 750))
        player.removeAction(forKey: "playerRunning")
    }
    
    func playerMoveLeft () {

        let rate: CGFloat = 0.50;
        let relativeVelocity = CGVector(dx: -500-(player.physicsBody?.velocity.dx)!, dy: (player.physicsBody?.velocity.dy)!);
        player.physicsBody?.velocity=CGVector(dx: (player.physicsBody?.velocity.dx)!+relativeVelocity.dx*rate, dy: relativeVelocity.dy);
        
    }
    
    func playerMoveRight() {

        let rate: CGFloat = 0.50;
        let relativeVelocity = CGVector(dx: 500-(player.physicsBody?.velocity.dx)!, dy: (player.physicsBody?.velocity.dy)!);
        player.physicsBody?.velocity=CGVector(dx: (player.physicsBody?.velocity.dx)!+relativeVelocity.dx*rate, dy: relativeVelocity.dy);
    }
    
    func bgMoveRight() {
        
        if (isCameraMoving() && playerIsMoving()) {
            
            let someScale: CGFloat = 100
            let moveXFactor: CGFloat = 3 * deltaTime * someScale
            
            let skyParallaxSpeedFactor: CGFloat = -0.2
            skyBg.position.x = skyBg.position.x + skyParallaxSpeedFactor * moveXFactor
            skyBgNext.position.x = skyBgNext.position.x + skyParallaxSpeedFactor * moveXFactor
            
            let montainsParallaxSpeedFactor: CGFloat = -0.4
            mountainsBg.position.x = mountainsBg.position.x + montainsParallaxSpeedFactor * moveXFactor
            mountainsBgNext.position.x = mountainsBgNext.position.x + montainsParallaxSpeedFactor * moveXFactor
        }
    }
    
    func bgMoveLeft() {
        
        if (isCameraMoving() && playerIsMoving()) {
            let someScale: CGFloat = 100
            let moveXFactor: CGFloat = 3 * deltaTime * someScale
            
            let skyParallaxSpeedFactor: CGFloat = -0.2
            skyBg.position.x = skyBg.position.x - skyParallaxSpeedFactor * moveXFactor
            skyBgNext.position.x = skyBgNext.position.x - skyParallaxSpeedFactor * moveXFactor
            
            let montainsParallaxSpeedFactor: CGFloat = -0.4
            mountainsBg.position.x = mountainsBg.position.x - montainsParallaxSpeedFactor * moveXFactor
            mountainsBgNext.position.x = mountainsBgNext.position.x - montainsParallaxSpeedFactor * moveXFactor
        }
    }
    
    func playerIsMoving() -> Bool {
        let playerPosition = Double(round(100*player.position.x)/100)
        let lastPlayerPosition = Double(round(100*self.lastPlayerPosition.x)/100)
        
        return (playerPosition != lastPlayerPosition)
    }
    
    func isCameraMoving () -> Bool {
        return (player.position.x > cameraMoveXStart && player.position.x < cameraMoveXEnd && (player.physicsBody?.velocity.dx)! as CGFloat != 0)
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        deltaTime = CGFloat(currentTime - lastUpdate)
        lastUpdate = currentTime
        
        if deltaTime > 1.0 {
            deltaTime = 0.0166
        }
        
        if shouldMoveLeft {
            playerMoveLeft()
            bgMoveLeft()
            
        }
        
        if shouldMoveRight {
            playerMoveRight()
            bgMoveRight()
        }
        
        lastPlayerPosition = player.position
    }
    
    func convert(point: CGPoint) -> CGPoint {
        print(self.view!.frame.height)
        return self.view!.convert(CGPoint(x: point.x, y:self.view!.frame.height-point.y), to:self.view)
    }
    
    func playerReset () {
        print("Player reset")
        player.physicsBody?.velocity = CGVector.zero
        player.position = playerStartPosition
        player.physicsBody?.collisionBitMask = GroundCategory | SceneEdgeCategory | EnemyCategory
        player.physicsBody?.contactTestBitMask = GroundCategory | SceneEdgeCategory | PitCategory | PitEntryCategory | EnemyCategory
        playerLives = 3
        livesLabel.text = "Lives \(playerLives)"
    }
    
}
